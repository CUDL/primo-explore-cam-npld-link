app.controller('camNpldController', [function () {
  var self = this;

  self.getDocid = getDocid();
  self.isNpld = isNpld();

  function getDocid() {
    return self.parentCtrl.result.pnx.control.recordid[0];
  }
    
  function isNpld() {
    return !!self.getDocid.match(/^44CAM_NPLD/);
  }

}]);
  
app.component('prmSearchResultAvailabilityLineAfter', {
  bindings: { parentCtrl: '<' },
  controller: 'camNpldController',
  template: '<span ng-if="$ctrl.isNpld" id="legal-deposit-explanation">UK legislation regulates access - <a href="https://libguides.cam.ac.uk/e-legal-deposit" id="legal-deposit-link" target="_blank">why?</a></span>'
});
